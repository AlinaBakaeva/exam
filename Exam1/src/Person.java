public class Person {
    private String name;
    private Gender gender;
    private int birthday;

    public Person(String name, Gender gender, int birthday) {
        this.name = name;
        this.gender = gender;
        this.birthday=birthday;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }
    @Override
    public String toString() {
        return "(" +
                " Фамилля - " + name + '\'' +
                ", Пол -" + gender +
                ')';
    }


    public int getBirthday() {
        return birthday;
    }
}



