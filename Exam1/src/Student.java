public class Student extends Person {
    private int year;
    private String specialty;

    public Student(String name, Gender gender, int birthday, int year, String specialty) {
        super(name,gender,birthday);
        this.year = year;
        this.specialty = specialty;
    }


    public int getYear() {
        return year;
    }

    public String getSpecialty() {
        return specialty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "Год поступления=" + year +
                ", специальность='" + specialty + '\'' +
                '}';
    }
}


