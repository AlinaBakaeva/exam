import com.sun.jmx.snmp.SnmpUnknownAccContrModelException;

import java.util.Scanner;
public class Demo {

    public static void main(String[] args) {
        Student student1 = new Student("Жданова", Gender.FEMALE, 2001, 2014, "09.02.03");
        Student student2 = new Student("Гарюнов", Gender.MALE, 2002, 2018, "09.02.03");
        Student student3 = new Student("Ковалко", Gender.FEMALE, 2002, 2018, "09.02.03");
        Student student4 = input();
        Student[] students = {student1, student2, student3, student4};
        System.out.println("Фамилии девочек поступивших в 2014 по специальности 09.02.03");
        girls(students);
    }

    private static void girls(Student[] students) {
        for (int i = 0; i < students.length; i++) {
            if (students[i].getYear() == 2014 && students[i].getGender() == Gender.FEMALE && students[i].getSpecialty().equals("09.02.03")) {
                System.out.println(students[i].getName());
            }
        }
    }


    private static Student input() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Фамилия студента: ");
        String name = scanner.nextLine();
        System.out.print("Пол(м или ж): ");
        String gender = scanner.nextLine();
        System.out.print("Год рождения: ");
        int year = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Год поступления: ");
        int birthday = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Код специальности: ");
        String specialty = scanner.nextLine();
        scanner.nextLine();
        Gender gender1 = Gender.FEMALE;
        if (gender.equalsIgnoreCase("м")) {
            gender1 = Gender.MALE;
        }
        return new Student(name, gender1, birthday, year, specialty);
    }
}


